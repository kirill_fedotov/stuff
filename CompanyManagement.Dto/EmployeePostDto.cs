﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyManagement.Dto
{
    public class EmployeePostDto
    {
        [Required]
        public int DepartmentId { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Range(0, Double.MaxValue)]
        public decimal Salary { get; set; } = 0;
        [Range(0, Double.MaxValue)]
        public double VacationDays { get; set; } = 0;
        public DateTime OnBoardDate { get; set; } = DateTime.UtcNow;
    }
}
