﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyManagement.Dto
{
    public class EmployeeGetDto
    {
        public int Id { get; set; }
        public DepartmentGetDto Department { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public decimal Salary { get; set; }
        public double VacationDays { get; set; }
        public DateTime OnBoardDate { get; set; }
    }
}
