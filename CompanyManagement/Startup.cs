﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CompanyManagement.Dto;
using CompanyManagement.Services;
using CompanyManagement.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Swagger;

namespace CompanyManagement
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddJsonOptions(options => {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("1.0", new Info { Title = "Company Manager API", Version = "1.0" });
            });
            services.AddOptions();
            services.AddDbContext<CompanyDbContext>(options => 
                    options.UseSqlServer(Configuration.GetConnectionString("CompanyDbContext"), 
                    x => x.MigrationsAssembly(typeof(CompanyDbContext).Assembly.GetName().Name)));
            services.AddTransient<EmployeeService>();
            services.AddTransient<DepartmentService>();
            services.AddAutoMapper(cfg => {
                cfg.AllowNullCollections = true;
                cfg.AllowNullDestinationValues = true;
                cfg.CreateMap<EmployeePostDto, Employee>();
                cfg.CreateMap<DepartmentPostDto, Department>();
                cfg.CreateMap<Employee, EmployeeGetDto>();
                cfg.CreateMap<Department, DepartmentGetDto>();
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<CompanyDbContext>();
                context.Database.Migrate();
            }

            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/1.0/swagger.json", "Company Management");
            });
        }
    }
}
