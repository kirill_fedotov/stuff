﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyManagement.Model
{
    public class CompanyDbContext : DbContext
    {
        public CompanyDbContext(DbContextOptions<CompanyDbContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Department>().HasMany(x => x.Employees).WithOne(x => x.Department)
                .HasForeignKey(x => x.DepartmentId);
            builder.Entity<Department>().HasKey(x => x.Id);
            builder.Entity<Department>().ToTable("Department");
            builder.Entity<Department>().Property(x => x.Id).UseSqlServerIdentityColumn();
            builder.Entity<Department>().Property(x => x.Name).IsRequired();


            builder.Entity<Employee>().HasKey(x => x.Id);
            builder.Entity<Employee>().ToTable("Employee");
            builder.Entity<Employee>().Property(x => x.Id).UseSqlServerIdentityColumn();
            builder.Entity<Employee>().Property(x => x.FirstName).IsRequired();
            builder.Entity<Employee>().Property(x => x.LastName).IsRequired();
            builder.Entity<Employee>().Property(x => x.Salary).HasColumnType("decimal(19,4)");

            //seed data
            var department1 = new Department() { Name = "department1", Id = 1 };
            var department2 = new Department() { Name = "department2", Id = 2 };
            var employee1 = new Employee()
            {
                FirstName = "employee1",
                MiddleName = "employee1",
                LastName = "employee1",
                Salary = 100000,
                VacationDays = 10,
                OnBoardDate = DateTime.UtcNow,
                DepartmentId = department1.Id,
                Id = 1
            };
            builder.Entity<Department>().HasData(department1);
            builder.Entity<Department>().HasData(department2);
            builder.Entity<Employee>().HasData(employee1);
        }

        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
}
