﻿using System;

namespace CompanyManagement.Model
{
    public class Employee
    {
        public int Id { get; set; }
        public int DepartmentId { get; set; }
        public virtual Department Department { get; set; }

        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public decimal Salary { get; set; }
        public double VacationDays { get; set; }
        public DateTime OnBoardDate { get; set; }
    }
}
