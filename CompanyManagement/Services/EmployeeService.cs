﻿using CompanyManagement.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyManagement.Services
{
    public class EmployeeService
    {
        private readonly CompanyDbContext _context;
        public EmployeeService(CompanyDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Employee> GetAll()
        {
            return _context.Employees.Include(x => x.Department);
        }
        public bool Exists(int id)
        {
            return _context.Employees.Any(x => x.Id == id);
        }
        public Employee GetById(int id)
        {
            return _context.Employees.Include(x => x.Department).FirstOrDefault(x=>x.Id == id);
        }
        public void AddEmployee(Employee employee)
        {
            employee.Id = 0;
            _context.Employees.Add(employee);
            _context.SaveChanges();
        }

        public void DeleteEmployee(int id)
        {
            var employee = new Employee() { Id = id };
            _context.Employees.Attach(employee);
            _context.Entry(employee).State = EntityState.Deleted;
            _context.SaveChanges();
        }
        public void TransferEmployee(int id, int newDepartmentId)
        {
            var employee = new Employee() { Id = id, DepartmentId = newDepartmentId};
            _context.Employees.Attach(employee);
            _context.Entry(employee).Property(x=>x.DepartmentId).IsModified = true;
            _context.SaveChanges();
        }
    }
}
