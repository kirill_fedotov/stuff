﻿using CompanyManagement.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyManagement.Services
{
    public class DepartmentService
    {
        private readonly CompanyDbContext _context;
        public DepartmentService(CompanyDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Department> GetAll()
        {
            return _context.Departments.Include(x=>x.Employees);
        }
        public bool Exists(int id)
        {
            return _context.Departments.Any(x => x.Id == id);
        }
        public Department GetById(int id)
        {
            return _context.Departments.Include(x => x.Employees).FirstOrDefault(x=>x.Id == id);
        }
        public void AddDepartment(Department department)
        {
            department.Id = 0;
            _context.Departments.Add(department);
            _context.SaveChanges();
        }
        public void DeleteDepartment(int id)
        {
            var department = new Department() { Id = id };
            _context.Departments.Attach(department);
            _context.Entry(department).State = EntityState.Deleted;
            _context.SaveChanges();
        }
    }
}
