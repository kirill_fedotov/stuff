﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CompanyManagement.Dto;
using CompanyManagement.Services;
using CompanyManagement.Model;
using Microsoft.AspNetCore.Mvc;

namespace CompanyManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeService _employeeService;
        private readonly DepartmentService _departmentService;
        private readonly IMapper _mapper;
        public EmployeeController(EmployeeService employeeService, 
            DepartmentService departmentService, IMapper mapper)
        {
            _employeeService = employeeService;
            _departmentService = departmentService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EmployeeGetDto>> Get()
        {
            var dto = _employeeService.GetAll().Select(x => _mapper.Map<Employee, EmployeeGetDto>(x));
            return Ok(dto);
        }
        [HttpGet("{id}")]
        public ActionResult<EmployeeGetDto> Get(int id)
        {
            var employee = _employeeService.GetById(id);
            if (employee == null) return NotFound();
            var dto = _mapper.Map<Employee, EmployeeGetDto>(employee);
            return Ok(dto);
        }
        [HttpPost]
        public ActionResult Post([FromBody] EmployeePostDto dto)
        {
            if (!_departmentService.Exists(dto.DepartmentId)) return BadRequest("Department does not exist");
            var employee = _mapper.Map<EmployeePostDto, Employee>(dto);
            _employeeService.AddEmployee(employee);
            return Ok();
        }
        [HttpPost("{id}/Transfer")]
        public ActionResult Transfer(int id, [FromBody] EmployeeTransferPostDto dto)
        {
            if (!_employeeService.Exists(id)) return NotFound();
            if (!_departmentService.Exists(dto.DepartmentId)) return NotFound();
            _employeeService.TransferEmployee(id, dto.DepartmentId);
            return Ok();
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (!_employeeService.Exists(id)) return NotFound();
            _employeeService.DeleteEmployee(id);
            return Ok();
        }
    }
}
