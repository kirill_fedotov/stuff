﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CompanyManagement.Dto;
using CompanyManagement.Services;
using CompanyManagement.Model;
using Microsoft.AspNetCore.Mvc;

namespace CompanyManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly DepartmentService _departmentService;
        private readonly IMapper _mapper;
        public DepartmentController(DepartmentService departmentService, IMapper mapper)
        {
            _departmentService = departmentService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<DepartmentGetDto>> Get()
        {
            var dto = _departmentService.GetAll().Select(x => _mapper.Map<Department, DepartmentGetDto>(x));
            return Ok(dto);
        }
        [HttpGet("{id}")]
        public ActionResult<DepartmentGetDto> Get(int id)
        {
            var department = _departmentService.GetById(id);
            if (department == null) return NotFound();
            var dto = _mapper.Map<Department, DepartmentGetDto>(department);
            return Ok(dto);
        }
        [HttpPost]
        public ActionResult Post([FromBody] DepartmentGetDto dto)
        {
            var department = _mapper.Map<DepartmentGetDto, Department>(dto);
            _departmentService.AddDepartment(department);
            return Ok();
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (!_departmentService.Exists(id)) return NotFound();
            if (_departmentService.GetById(id).Employees.Any()) return BadRequest("Department has employees");
            _departmentService.DeleteDepartment(id);
            return Ok();
        }
    }
}
