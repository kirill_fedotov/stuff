﻿using CompanyManagement.Web.Services.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CompanyManagement.Web.Services
{
    public class ApiService
    {
        public ApiEndpointConfiguration Configuration { get; }
        private readonly string _apiUri;
        private readonly HttpClient _client;
        public ApiService(IOptions<ApiEndpointConfiguration> configuration)
        {
            Configuration = configuration.Value;
            _apiUri = $"{Configuration.Host}:{Configuration.Port}/{Configuration.Path}";
            _client = new HttpClient();
        }
        public T Get<T>(string route)
        {
            var result = _client.GetAsync(_apiUri + route).Result;
            if (result.IsSuccessStatusCode)
                return result.Content.ReadAsAsync<T>().Result;
            else
                throw new Exception($"{result.StatusCode}");
        }
        public void Delete(string route)
        {
            var result = _client.DeleteAsync(_apiUri + route).Result;
            if (!result.IsSuccessStatusCode)
                throw new Exception($"{result.StatusCode}");
        }
        public void Post<T>(string route, T data)
        {
            var content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            var result = _client.PostAsync(_apiUri + route, content).Result;
            if (!result.IsSuccessStatusCode)
                throw new Exception($"{result.StatusCode}");
        }
    }
}
