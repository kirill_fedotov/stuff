﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompanyManagement.Web.Services.Models
{
    public class ApiEndpointConfiguration
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Path { get; set; }
    }
}
