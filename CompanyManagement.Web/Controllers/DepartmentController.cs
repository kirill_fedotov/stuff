﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CompanyManagement.Dto;
using CompanyManagement.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CompanyManagement.Web.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly ApiService _apiService;
        public DepartmentController(ApiService apiService)
        {
            _apiService = apiService;
        }

        public ActionResult Index()
        {
            var dto = _apiService.Get<List<DepartmentGetDto>>("Department");
            return View(dto);
        }

        public ActionResult Details(int id)
        {
            var dto = _apiService.Get<DepartmentGetDto>($"Department/{id}");
            return View(dto);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DepartmentPostDto dto)
        {
            try
            {
                _apiService.Post($"Department", dto);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            var dto = _apiService.Get<DepartmentGetDto>($"Department/{id}");
            return View(dto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                _apiService.Delete($"Department/{id}");
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}