﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CompanyManagement.Dto;
using CompanyManagement.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CompanyManagement.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly ApiService _apiService;
        public EmployeeController(ApiService apiService)
        {
            _apiService = apiService;
        }

        public ActionResult Index()
        {
            var dto = _apiService.Get<List<EmployeeGetDto>>("Employee");
            return View(dto);
        }

        public ActionResult Details(int id)
        {
            var dto = _apiService.Get<EmployeeGetDto>($"Employee/{id}");
            return View(dto);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EmployeePostDto dto)
        {
            try
            {
                _apiService.Post($"Employee", dto);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        
        public ActionResult Delete(int id)
        {
            var dto = _apiService.Get<EmployeeGetDto>($"Employee/{id}");
            return View(dto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                _apiService.Delete($"Employee/{id}");
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Transfer(int id)
        {
            var dto = _apiService.Get<EmployeeGetDto>($"Employee/{id}");
            return View(new EmployeeTransferPostDto() { DepartmentId = dto.Department.Id});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Transfer(int id, EmployeeTransferPostDto dto)
        {
            try
            {
                _apiService.Post($"Employee/{id}/Transfer", dto);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}